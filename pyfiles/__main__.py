import sys
import json
import argparse
from datetime import datetime
from sqlalchemy import create_engine

from .ranking_functions import load_model, process, dump_report

parser = argparse.ArgumentParser(description = "Ranking creator - Main", epilog = "See you later")
parser.add_argument("path_to_config", type = str, help = "Path to JSON file with connectors")
parser.add_argument("--date", type = str, default = None, help = "Date for service invocation (default: today, format: YYYY-MM-DD)" )

args = parser.parse_args()

try:
    with open(args.path_to_config, "r") as config:
        config_dict = json.load(config)
except Exception as E:
    print(f"Exception {E} occurred during loading of the configuration file -- Now closing")
    sys.exit()

try:
    importances = load_model(path = config_dict["model"]["path"])
except Exception as E:
    print(f"Exception {E} occurred during loading of the importances -- Now closing")
    sys.exit()

connector_strings = config_dict["databases"]

try:
    connectors = {
        "conn_bi" : create_engine(connector_strings["db_bi"], pool_recycle = 3600).connect(),
        "conn_di" : create_engine(connector_strings["db_di"], pool_recycle = 3600).connect(),
    }
except Exception as E:
    print(f"Exception {E} occurred during creation of the connectors -- Now closing")
    sys.exit()

date_str = args.date

if date_str is None:
    my_date = datetime.now()
else:
    my_date = datetime.strptime(date_str, '%Y-%m-%d')

results = process(connectors, importances, {"date" : my_date}, log = lambda x : print(x))

if results != None:
    rankings, timestamps = results

    with open(f"rankings_{my_date.strftime('%Y_%m_%d')}.json", "w") as f:
        json.dump(rankings, f)

    print()
    dump_report(timestamps)
    print()
else:
    print("An exception occurred")