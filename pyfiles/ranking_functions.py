import json
import traceback as tb

import numpy as np
import pandas as pd

from constants import mapping_c2m, extracted_features, real, chunk_size
from datetime import datetime, timedelta

macrocode_window_days = {
    "Energia FA" : 20,
    "Energia Commerciali" : 18,
    "Telefonia FA" : 18,
    "Telefonia Commerciali" : 20
}

def load_model(path):
    importances = {}
    for macrocoda, window in macrocode_window_days.items():
        name = macrocoda.lower().replace(" ", "_")
        with open(f"{path}/importances_window_{window}_macrocoda_{name}_RR_09.json") as f:
            importances[macrocoda] = json.load(f)
    return importances

# ------------------------------------------------------------------------------------------------------------------------------------------- #

def process(connectors, model, input, log):
    my_date = input["date"]

    timestamps = {}
    timestamps["start"] = datetime.now()

    try:
        orders, calls, user_team_vw, prenotazioni, consulenti = collect_dataframes(connectors, my_date)
    except Exception as E:
        log(f"@ 'data collection' generated traceback {tb.format_exc()}. 'None' dataframe will be returned")
        return None

    timestamps["data collection"] = datetime.now()

    try:
        consulenti = sanitize_consulenti(consulenti)
    except Exception as E:
        log(f"@ 'sanification of column stato in consulenti' generated traceback {tb.format_exc()}. 'None' dataframe will be returned")
        return None

    timestamps["sanification of column stato in consulenti"] = datetime.now()

    try:
        bi_dataframe = process_bi_tables(calls, orders, user_team_vw)
    except Exception as E:
        log(f"@ 'process BI tables' generated traceback {tb.format_exc()}. 'None' dataframe will be returned")
        return None

    timestamps["process BI tables"] = datetime.now()

    try:
        macrocode_bi = generate_macrocode_datasets(bi_dataframe, user_team_vw)
    except Exception as E:
        log(f"@ 'separation in macrocode' generated traceback {tb.format_exc()}. 'None' dataframe will be returned")
        return None

    timestamps["separation in macrocode"] = datetime.now()

    try:
        di_dataframe = process_di_tables(consulenti, prenotazioni)
    except Exception as E:
        log(f"@ 'process DI tables' generated traceback {tb.format_exc()}. 'None' dataframe will be returned")
        return None

    timestamps["process DI tables"] = datetime.now()

    try:
        unified_bi_di = join_macrocode_bi_and_di_dataframe(macrocode_bi, di_dataframe)
    except Exception as E:
        log(f"@ 'unification BI and DI' generated traceback {tb.format_exc()}. 'None' dataframe will be returned")
        return None

    timestamps["unification BI and DI"] = datetime.now()

    try:
        rankings = generate_operator_ranking(unified_bi_di, model, my_date)
    except Exception as E:
        log(f"@ 'generation of ranking' generated traceback {tb.format_exc()}. 'None' dataframe will be returned")
        return None

    timestamps["generation of rankings"] = datetime.now()

    return (rankings, timestamps)

# ------------------------------------------------------------------------------------------------------------------------------------------- #

def dump_report(timestamps):
    checkpoints = list(timestamps.keys())
    print()
    print(f"@ {timestamps['start'].strftime('%H-%M-%S')} Start")

    total_duration = 0
    for i in range(1, len(checkpoints)):
        curr_ckp = checkpoints[i]
        prev_ckp = checkpoints[i-1]
        difference = (timestamps[curr_ckp] - timestamps[prev_ckp]).total_seconds()
        print(f"From '{prev_ckp}' to '{curr_ckp}' elapsed {difference} seconds")
        total_duration += difference

    print("Total duration: ", round(total_duration, 3), "seconds")

# ------------------------------------------------------------------------------------------------------------------------------------------- #

def generate_operator_ranking(uni_bi_di, importances, my_date):

    rankings = []

    for macrocoda, uni_df in uni_bi_di.items():

        df_history = uni_df.copy()
        df_history.insert(0, "perc_ko_cumul_avg_last", df_history.groupby("id_consulente")["perc_ko_cumul_avg"].transform("last"), allow_duplicates = True)
        df_history = df_history.drop_duplicates(subset = "id_consulente", keep = "first").reset_index(drop = True)

        day_df_history = df_history[["id_consulente", "perc_ko_cumul_avg_last"]]

        df_window = uni_df.copy()
        df_window = df_window[df_window["data"] >= pd.to_datetime(my_date - timedelta(days = macrocode_window_days[macrocoda]))]

        for feature in extracted_features:
            df_window.insert(0, feature + "_medio", df_window.groupby("id_consulente")[feature].transform("mean"), allow_duplicates = True)

        df_window.drop(columns = extracted_features + ["data", "perc_ko"], inplace = True)
        day_df_window = df_window.drop_duplicates(subset = "id_consulente", keep = "first").reset_index(drop = True)

        day_df = pd.merge(day_df_history, day_df_window, how = "inner", on = ["id_consulente"])

        for col in day_df.columns:
            if any(s in col for s in ["ko", "cancellat", "non", "pausa"]):
                day_df[col] = -day_df[col]

        day_df["score"] = day_df.apply(lambda row: scoring(row, importances[macrocoda], f_RR = 0.9), axis = 1)

        ranking = day_df[["id_consulente", "score"]].sort_values(by = "score", ignore_index = True, ascending = False)
        ranking.insert(0, "macrocoda", macrocoda.lower().replace(" ", "_"), allow_duplicates = True)
        
        rankings.extend(ranking.to_dict("records"))

    rankings = sorted(rankings, key = lambda d: str(d["id_consulente"]) + d["macrocoda"])

    return rankings

# ------------------------------------------------------------------------------------------------------------------------------------------- #

def join_macrocode_bi_and_di_dataframe(macrocode_bi, di_dataframe):

    unified_bi_di = {}

    di_dataframe["data"] = di_dataframe["data"].astype(str)

    for macrocoda, bi_df in macrocode_bi.items():

        bi_df["data"] = bi_df["data"].astype(str)

        uni_df = pd.merge(bi_df[['data', 'RR', 'perc_ko', 'id_consulente']], di_dataframe, how = "inner", on = ["data", "id_consulente"])
        uni_df["data"] = uni_df["data"].apply(pd.to_datetime)

        uni_df.insert(0, "perc_ko_cumul_sum", uni_df.groupby("id_consulente")["perc_ko"].transform("cumsum"), allow_duplicates = True)
        uni_df.insert(0, "perc_ko_cumul_count", uni_df.groupby("id_consulente")["perc_ko"].transform("cumcount") + 1, allow_duplicates = True)
        uni_df.insert(0, "perc_ko_cumul_avg", uni_df["perc_ko_cumul_sum"] / uni_df["perc_ko_cumul_count"], allow_duplicates = True)
        uni_df["perc_ko_cumul_avg"] = uni_df["perc_ko_cumul_avg"].fillna(0)

        uni_df.drop(columns = ['perc_ko_cumul_sum', 'perc_ko_cumul_count'], inplace = True)

        unified_bi_di[macrocoda] = uni_df

    return unified_bi_di

# ------------------------------------------------------------------------------------------------------------------------------------------- #

def generate_macrocode_datasets(bi_dataframe, user_team_vw):

    macrocode_bi = {}

    for macrocoda in bi_dataframe["macrocoda"].unique():

        if macrocoda is None or "cross" in macrocoda:
            continue

        df = bi_dataframe[bi_dataframe["macrocoda"] == macrocoda]

        df = df.drop_duplicates(subset = ['data','username'])
        df = df.merge(user_team_vw[['username','id']], on = 'username', how = 'left')
        df = df.rename(columns = {"id":"id_consulente"}).reset_index(drop = True)
        df["perc_ko"] = df["perc_ko"].fillna(0)

        macrocode_bi[macrocoda] = df

    return macrocode_bi

# ------------------------------------------------------------------------------------------------------------------------------------------- #

def process_di_tables(consulenti, prenotazioni):

    consulenti.insert(0, "data", consulenti["inizio"].dt.date, allow_duplicates = True)
    consulenti.insert(0, "durata", (consulenti["fine"] - consulenti["inizio"]).dt.total_seconds(), allow_duplicates = True)

    report_consultants = featurize_consultants(consulenti)

    prenotazioni.insert(0, "data", prenotazioni["data_creazione"].dt.date, allow_duplicates = True)

    df_outs = []
    outcomes = sorted(prenotazioni["esito"].unique())
    mapping = dict(zip(outcomes, ["data_accettazione", "data_cancellazione", "data_timeout"]))

    for OUT in outcomes:
        df_out = prenotazioni[prenotazioni["esito"] == OUT]
        df_out.insert(0, "durata", (df_out[mapping[OUT]] - df_out["data_creazione"]).dt.total_seconds(), allow_duplicates = True)
        df_outs.append(df_out)

    prenotazioni = pd.concat(df_outs)

    report_reservation = featurize_reservations(prenotazioni)

    di_df = pd.merge(report_consultants, report_reservation, how = 'inner', on = ['data', 'id_consulente'])

    return di_df

# ------------------------------------------------------------------------------------------------------------------------------------------- #

def process_bi_tables(calls, orders, user_team_vw):

    orders.insert(0, "order", np.where(orders["stato_redemption"] == 'ok', 1, 0), allow_duplicates = True)
    orders.insert(0, "ko_order", np.where(orders["stato_redemption"] == 'ko', 1, 0), allow_duplicates = True)

    all_redemption = pd.merge(calls, orders, on = 'call_id', how = 'left')

    all_redemption['order'] = all_redemption['order'].fillna(0)
    all_redemption['ko_order'] = all_redemption['ko_order'].fillna(0)
    all_redemption.insert(0, "call", 1, allow_duplicates = True)
    all_redemption.insert(0, "macrocoda", all_redemption["coda_in_ingresso"].replace(mapping_c2m), allow_duplicates = True)

    all_redemption = all_redemption[~((all_redemption["username_operatore_x"].isna()) &\
                                      (all_redemption["username_operatore_y"].isna()))]

    all_redemption["username"] = np.where(all_redemption["username_operatore_x"] == all_redemption["username_operatore_y"],
                                          all_redemption["username_operatore_x"],
                                          np.nan)

    all_redemption["username"] = np.where(all_redemption["username_operatore_x"].isna(),
                                          all_redemption["username_operatore_y"],
                                          all_redemption["username"])

    all_redemption["username"] = np.where(all_redemption["username_operatore_y"].isna(),
                                          all_redemption["username_operatore_x"],
                                          all_redemption["username"])

    all_redemption["username"] = np.where(((all_redemption["username"].isna()) &\
                                           (all_redemption["username_operatore_x"] != all_redemption["username_operatore_y"]) &\
                                           (all_redemption["username_operatore_x"].str[:4] + all_redemption["username_operatore_x"].str[-2:] == all_redemption["username_operatore_y"])),
                                          all_redemption["username_operatore_x"],
                                          all_redemption["username"])

    all_redemption["username"] = np.where(((all_redemption["username"].isna()) &\
                                           (all_redemption["username_operatore_x"] != all_redemption["username_operatore_y"]) &\
                                           (all_redemption["username_operatore_x"].str[:4] + all_redemption["username_operatore_x"].str[-2:] != all_redemption["username_operatore_y"])),
                                          all_redemption["username_operatore_y"],
                                          all_redemption["username"])

    all_redemption["username"] = np.where(((all_redemption["username"].astype(str).str.contains("Pat") | all_redemption["username"].astype(str).str.contains("pat")) & (all_redemption["username"].str.len() == 6)),
                                          all_redemption["username"].str[:4] + "energia10" + all_redemption["username"].str[-2:],
                                          all_redemption["username"])

    all_redemption = all_redemption.replace({"username": real})
    all_redemption = all_redemption[~all_redemption["username"].isna()]
    all_redemption = all_redemption[all_redemption["username"].isin(user_team_vw["username"])]

    # computing on a daily base number of orders (ok and ko) and all_redemption rate (RR)

    all_redemption = all_redemption[["data", "username", "order","ko_order", "call", "macrocoda", "coda_in_ingresso"]]

    aggregated_redemption = all_redemption.groupby(["macrocoda", "username","data"])

    all_redemption.insert(0, "num_ord", aggregated_redemption["order"].transform("sum"), allow_duplicates = True)
    all_redemption.insert(0, "num_ko_ord", aggregated_redemption["ko_order"].transform("sum"), allow_duplicates = True)
    all_redemption.insert(0, "num_cal", aggregated_redemption["call"].transform("sum"), allow_duplicates = True)
    all_redemption.insert(0, "RR", all_redemption["num_ord"] / all_redemption["num_cal"], allow_duplicates = True)
    all_redemption.insert(0, "perc_ko", all_redemption["num_ko_ord"]/(all_redemption["num_ord"] + all_redemption["num_ko_ord"]), allow_duplicates = True)

    return all_redemption

# ------------------------------------------------------------------------------------------------------------------------------------------- #

def sanitize_consulenti(df):

    df["stato"] = df["stato"].replace(["Pausa 626", "Pausa BO"], ["Pausa", "Pausa"])
    df["stato"] = df["stato"].apply(lambda s: s.lower().replace(" ", "_"))

    return df

# ------------------------------------------------------------------------------------------------------------------------------------------- #

def collect_dataframes(connectors, my_date):

    EDATE  = my_date + timedelta(days = 1)
    SDATE1 = EDATE - timedelta(days = 30)
    SDATE2 = EDATE - timedelta(days = 20)

    connector_bi = connectors["conn_bi"]
    connector_di = connectors["conn_di"]

    orders = get_ft_venduto(connector_bi, SDATE1, EDATE)
    calls = get_ft_chiamate(connector_bi, SDATE1, EDATE)

    user_team_vw = get_user_team_view(connector_di)
    prenotazioni = get_inbound_prenotazioni(connector_di, SDATE2, EDATE)
    consulenti = get_consulenti_stato_audit(connector_di, SDATE2, EDATE)

    return orders, calls, user_team_vw, prenotazioni, consulenti

# ------------------------------------------------------------------------------------------------------------------------------------------------- #
# ------------------------------------------------------------------------------------------------------------------------------------------------- #

## --------------- dataframe getters from database - BEGIN

def get_user_team_view(connector):

    user_team_vw = pd.read_sql(f"SELECT * FROM user_team_vw", connector)

    if user_team_vw.shape[0] == 0:
        raise Exception("Dataframe Extracted from 'user_team_vw' has no rows")

    return user_team_vw

def get_ft_chiamate(connector, SDATE, EDATE):

    # data, call_id, username_operatore, coda_in_ingresso
    query_calls = f"SELECT * FROM ft_chiamate WHERE data_creazione > '{SDATE}' AND data_creazione < '{EDATE}' AND direzione LIKE 'Inbound'"

    calls = pd.concat([chunk for chunk in pd.read_sql(query_calls, connector, chunksize = chunk_size)])

    if calls.shape[0] == 0:
        raise Exception ("Dataframe extracted from 'ft_chiamate' has no rows")

    return calls

def get_ft_venduto(connector, SDATE, EDATE):

    # call_id, stato_redemption, username_operatore, tipo_contratto, data_creazione
    query_orders = f"SELECT * FROM ft_venduto WHERE (data_creazione > '{SDATE}' AND data_creazione < '{EDATE}') AND (tipo_prodotto LIKE 'Utility')"

    orders = pd.concat([chunk for chunk in pd.read_sql(query_orders, connector, chunksize = chunk_size)])

    if orders.shape[0] == 0:
        raise Exception("Dataframe extracted from 'ft_venduto' has no rows")

    return orders

def get_inbound_prenotazioni(connector, SDATE, EDATE):

    # id_consulente, esito, data_creazione, data_accettazione, data_timeout, data_cancellazione
    query_prenot = f"""SELECT * FROM inbound_prenotazioni WHERE esito != "" AND partner_id = 1 AND (data_creazione > '{SDATE}' AND data_creazione < '{EDATE}')"""

    prenotazioni = pd.concat([chunk for chunk in pd.read_sql(query_prenot, connector, chunksize = chunk_size)])

    if prenotazioni.shape[0] == 0:
        raise  Exception("Dataframe extracted from 'inbound_prenotazioni' has no rows")

    return prenotazioni

def get_consulenti_stato_audit(connector, SDATE, EDATE):

    l = ['predictive', 'seleziona modalità', 'Non collegato', 'Esito chiamata']

    st = ""
    for s in l:
        st += "'" + s + "', "

    stati = st[:-2]

    query_consultants = f"SELECT * FROM consulenti_stato_audit WHERE (inizio > '{SDATE}' AND inizio < '{EDATE}') AND stato NOT IN ({stati})"

    consulenti = pd.concat([chunk for chunk in pd.read_sql(query_consultants, connector, chunksize = chunk_size)])

    if consulenti.shape[0] == 0:
        raise Exception("Dataframe extracted from 'consulenti_stato_audit' has no rows")

    return consulenti

## --------------- dataframe getters from database - END

## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##

def unfold_pivot(summary, col_state, columns):

    """In 'values' there are the extracted features chosen this form to apply the "flattening of the columns after pivoting"""

    values = columns[3:]
    report = summary.pivot(index = ["data", "id_consulente"], columns = col_state, values = values).fillna(0).reset_index()
    report.columns = ['_'.join(col).rstrip("_") for col in report.columns]

    return report

## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##

## --------------- dataframe from consulenti_stato_audit - BEGIN

def featurize_consultants(df):

    df.insert(0, "durata_totale", df.groupby(["data", "id_consulente"])["durata"].transform("sum"), allow_duplicates = True)
    df.insert(0, "durata_totale_stato", df.groupby(["data", "id_consulente", "stato"])["durata"].transform("sum"), allow_duplicates = True)
    df.insert(0, "consulenti_rapporto_su_totale_stato", df["durata_totale_stato"] / df["durata_totale"], allow_duplicates = True)

    my_columns = ["data", "id_consulente", "stato", "consulenti_rapporto_su_totale_stato"]
    summary = df[my_columns].drop_duplicates(subset = ["data", "id_consulente", "stato"], keep = "first").reset_index(drop = True)

    return unfold_pivot(summary, col_state = "stato", columns = my_columns)

## --------------- dataframe from consulenti_stato_audit - END

## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##

## --------------- dataframe from inbound_prenotazioni - BEGIN

def featurize_reservations(df):

    df.insert(0, "numero_chiamate", df.groupby(["data", "id_consulente"])["id_consulente"].transform("count"), allow_duplicates = True)
    df.insert(0, "numero_chiamate_esito", df.groupby(["data", "id_consulente", "esito"])["esito"].transform("count"), allow_duplicates = True)
    df.insert(0, "prenotazioni_rapporto_su_totale_chiamate_esito", df["numero_chiamate_esito"] / df["numero_chiamate"], allow_duplicates = True)

    df.insert(0, "durata_totale", df.groupby(["data", "id_consulente"])["durata"].transform("sum"), allow_duplicates = True)
    df.insert(0, "durata_totale_esito", df.groupby(["data", "id_consulente", "esito"])["durata"].transform("sum"), allow_duplicates = True)
    df.insert(0, "prenotazioni_rapporto_su_totale_durata_esito", df["durata_totale_esito"] / df["durata_totale"], allow_duplicates = True)

    my_columns = ["data", "id_consulente", "esito", "prenotazioni_rapporto_su_totale_chiamate_esito", "prenotazioni_rapporto_su_totale_durata_esito"]
    summary = df[my_columns].drop_duplicates(subset = ["data", "id_consulente", "esito"], keep = "first")

    return unfold_pivot(summary, col_state = "esito", columns = my_columns)

## --------------- dataframe from inbound_prenotazioni - END

## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##

# computing scores for different weight of 'RR_medio'

def scoring(row, importances, f_RR):

    """
    Score = f_RR * RR_medio + sum from 1 to Nfeats Weighted feature value
    weight of feature = [(1 - f_RR) / sum of all importances] x importance of the feature
    """

    total_importance = sum(importances.values())
    factor = (1 - f_RR) / total_importance

    return f_RR*row["RR_medio"] + sum([factor*I*row[col] for col, I in importances.items()])