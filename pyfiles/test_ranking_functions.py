import sys, os
import json
import sqlite3
import unittest

from numpy import dtype
from datetime import date
from sqlalchemy import create_engine

from constants import *
from ranking_functions import *

results_path = "../data/snapshots/rankings_2022_04_04_at_2022_04_05.json"
snapshot_path = "../data/snapshots/data_04_04_2022_at_2022_04_05.sqlite"

my_date_ok = date(2022,  4,  4)
my_date_ko = date(2022, 12, 31)

class RankingTest(unittest.TestCase):

    def setUp(self):
        self.importances = load_model(path = "../data/models")
        connection_string = f"sqlite:///{snapshot_path}"
        self.connectors = {
            "conn_bi" : create_engine(connection_string, connect_args = dict(detect_types = sqlite3.PARSE_DECLTYPES)).connect(),
            "conn_di" : create_engine(connection_string, connect_args = dict(detect_types = sqlite3.PARSE_DECLTYPES)).connect()
        }

    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #
    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #
    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #
    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #

    def test_pulled_tables_ok(self):

        orders, calls, user_team_vw, prenotazioni, consulenti = collect_dataframes(self.connectors, my_date_ok)

        # ---------------------------------------------------- orders from ft_venduto

        self.assertNotEqual(orders.shape[0], 0, f"Table extracted from table of 'ft_venduto' has no rows")
        self.assertTrue(all(col in orders.columns for col in dict_employed["ft_venduto"]), f"Minimum columns are missing from table of 'ft_venduto'")
        self.assertEqual(orders.shape, dict_shapes["ft_venduto"], f"Shapes of dataframe from 'ft_venduto' does not match with original shape")
        self.assertEqual(orders.dtypes.to_dict(), dict_types["ft_venduto"], f"Dtypes of dataframe from 'ft_venduto' does not match with original")

        # ---------------------------------------------------- calls from ft_chiamate

        self.assertNotEqual(calls.shape[0], 0, f"Table extracted from table of 'ft_chiamate' has no rows")
        self.assertTrue(all(col in calls.columns for col in dict_employed["ft_chiamate"]), f"Minimum columns are missing from table of 'ft_chiamate'")
        self.assertEqual(calls.shape, dict_shapes["ft_chiamate"], f"Shapes of dataframe from 'ft_chiamate' does not match with original shape")
        self.assertEqual(calls.dtypes.to_dict(), dict_types["ft_chiamate"], f"Dtypes of dataframe from 'ft_chiamate' does not match with original")

        # ---------------------------------------------------- user_team_vw from user_team_vw

        self.assertNotEqual(user_team_vw.shape[0], 0, f"Table extracted from table of 'user_team_vw' has no rows")
        self.assertTrue(all(col in user_team_vw.columns for col in dict_employed["user_team_vw"]), f"Minimum columns are missing from table of 'user_team_vw'")
        self.assertEqual(user_team_vw.shape, dict_shapes["user_team_vw"], f"Shapes of dataframe from 'user_team_vw' does not match with original shape")
        self.assertEqual(user_team_vw.dtypes.to_dict(), dict_types["user_team_vw"], f"Dtypes of dataframe from 'user_team_vw' does not match with original ")

        # ---------------------------------------------------- prenotazioni from inbound_prenotazioni

        self.assertNotEqual(prenotazioni.shape[0], 0, f"Table extracted from table of 'inbound_prenotazioni' has no rows")
        self.assertTrue(all(col in prenotazioni.columns for col in dict_employed["inbound_prenotazioni"]), f"Minimum columns are missing from table of 'inbound_prenotazioni'")
        self.assertEqual(prenotazioni.shape, dict_shapes["inbound_prenotazioni"], f"Shapes of dataframe from 'inbound_prenotazioni' does not match with original shape")
        self.assertEqual(prenotazioni.dtypes.to_dict(), dict_types["inbound_prenotazioni"], f"Dtypes of dataframe from 'inbound_prenotazioni' does not match with original")

        self.assertTrue(prenotazioni["partner_id"].unique() == 1, "The field 'partner_id' of 'inbound_prenotazioni' contains elements different than 1")

        expected_outcomes = ["accettata", "cancellata", "timeout"]
        self.assertEqual(sorted(prenotazioni["esito"].unique()), sorted(expected_outcomes), "Values of 'esito' from 'inbound_prenotazioni' are not matching the expected ones")

        # ---------------------------------------------------- consulenti from consulenti_stato_audit

        self.assertNotEqual(consulenti.shape[0], 0, f"Table extracted from table of 'consulenti_stato_audit' has no rows")
        self.assertTrue(all(col in consulenti.columns for col in dict_employed["consulenti_stato_audit"]), f"Minimum columns are missing from table of 'consulenti_stato_audit'")
        self.assertEqual(consulenti.shape, dict_shapes["consulenti_stato_audit"], f"Shapes of dataframe from 'consulenti_stato_audit' does not match with original shape")
        self.assertEqual(consulenti.dtypes.to_dict(), dict_types["consulenti_stato_audit"], f"Dtypes of dataframe from 'consulenti_stato_audit' does not match with original")

    def test_pulled_tables_ko(self):

        EDATE  = my_date_ko + timedelta(days = 1)

        self.assertRaises(Exception, collect_dataframes, self.connectors, my_date_ko)

    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #
    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #
    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #
    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #

    def test_ft_venduto_ok(self):

        EDATE  = my_date_ok + timedelta(days = 1)

        SDATE1 = EDATE - timedelta(days = 30)
        SDATE2 = EDATE - timedelta(days = 20)

        connector_bi = self.connectors["conn_bi"]

        orders = get_ft_venduto(connector_bi, SDATE1, EDATE)

        self.assertNotEqual(orders.shape[0], 0, f"Table extracted from table of 'ft_venduto' has no rows")
        self.assertTrue(all(col in orders.columns for col in dict_employed["ft_venduto"]), f"Minimum columns are missing from table of 'ft_venduto'")
        self.assertEqual(orders.shape, dict_shapes["ft_venduto"], f"Shapes of dataframe from 'ft_venduto' does not match with original shape")
        self.assertEqual(orders.dtypes.to_dict(), dict_types["ft_venduto"], f"Dtypes of dataframe from 'ft_venduto' does not match with original")

    def test_ft_venduto_ko(self):

        EDATE  = my_date_ko + timedelta(days = 1)

        SDATE1 = EDATE - timedelta(days = 30)

        connector_bi = self.connectors["conn_bi"]

        self.assertRaises(Exception, get_ft_venduto, connector_bi, SDATE1, EDATE)

    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #

    def test_ft_chiamate_ok(self):

        EDATE  = my_date_ok + timedelta(days = 1)

        SDATE1 = EDATE - timedelta(days = 30)

        connector_bi = self.connectors["conn_bi"]

        calls = get_ft_chiamate(connector_bi, SDATE1, EDATE)

        self.assertNotEqual(calls.shape[0], 0, f"Table extracted from table of 'ft_chiamate' has no rows")
        self.assertTrue(all(col in calls.columns for col in dict_employed["ft_chiamate"]), f"Minimum columns are missing from table of 'ft_chiamate'")
        self.assertEqual(calls.shape, dict_shapes["ft_chiamate"], f"Shapes of dataframe from 'ft_chiamate' does not match with original shape")
        self.assertEqual(calls.dtypes.to_dict(), dict_types["ft_chiamate"], f"Dtypes of dataframe from 'ft_chiamate' does not match with original")

    def test_ft_chiamate_ko(self):

        EDATE  = my_date_ko + timedelta(days = 1)

        SDATE1 = EDATE - timedelta(days = 30)

        connector_bi = self.connectors["conn_bi"]

        self.assertRaises(Exception, get_ft_chiamate, connector_bi, SDATE1, EDATE)

    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #

    def test_user_team_vw_ok(self):

        connector_di = self.connectors["conn_bi"]

        user_team_vw = get_user_team_view(connector_di)

        self.assertNotEqual(user_team_vw.shape[0], 0, f"Table extracted from table of 'user_team_vw' has no rows")
        self.assertTrue(all(col in user_team_vw.columns for col in dict_employed["user_team_vw"]), f"Minimum columns are missing from table of 'user_team_vw'")
        self.assertEqual(user_team_vw.shape, dict_shapes["user_team_vw"], f"Shapes of dataframe from 'user_team_vw' does not match with original shape")
        self.assertEqual(user_team_vw.dtypes.to_dict(), dict_types["user_team_vw"], f"Dtypes of dataframe from 'user_team_vw' does not match with original ")

    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #

    def test_inbound_prenotazioni_ok(self):

        EDATE  = my_date_ok + timedelta(days = 1)

        SDATE2 = EDATE - timedelta(days = 20)

        connector_di = self.connectors["conn_di"]

        prenotazioni = get_inbound_prenotazioni(connector_di, SDATE2, EDATE)

        self.assertNotEqual(prenotazioni.shape[0], 0, f"Table extracted from table of 'inbound_prenotazioni' has no rows")
        self.assertTrue(all(col in prenotazioni.columns for col in dict_employed["inbound_prenotazioni"]), f"Minimum columns are missing from table of 'inbound_prenotazioni'")
        self.assertEqual(prenotazioni.shape, dict_shapes["inbound_prenotazioni"], f"Shapes of dataframe from 'inbound_prenotazioni' does not match with original shape")
        self.assertEqual(prenotazioni.dtypes.to_dict(), dict_types["inbound_prenotazioni"], f"Dtypes of dataframe from 'inbound_prenotazioni' does not match with original")


    def test_inbound_prenotazioni_ko(self):

        EDATE  = my_date_ko + timedelta(days = 1)

        SDATE2 = EDATE - timedelta(days = 20)

        connector_di = self.connectors["conn_di"]

        self.assertRaises(Exception, get_inbound_prenotazioni, connector_di, SDATE2, EDATE)

    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #

    def test_consulenti_stato_audit_ok(self):

        EDATE  = my_date_ok + timedelta(days = 1)

        SDATE2 = EDATE - timedelta(days = 20)

        connector_di = self.connectors["conn_di"]

        consulenti = get_consulenti_stato_audit(connector_di, SDATE2, EDATE)

        self.assertNotEqual(consulenti.shape[0], 0, f"Table extracted from table of 'consulenti_stato_audit' has no rows")
        self.assertTrue(all(col in consulenti.columns for col in dict_employed["consulenti_stato_audit"]), f"Minimum columns are missing from table of 'consulenti_stato_audit'")
        self.assertEqual(consulenti.shape, dict_shapes["consulenti_stato_audit"], f"Shapes of dataframe from 'consulenti_stato_audit' does not match with original shape")
        self.assertEqual(consulenti.dtypes.to_dict(), dict_types["consulenti_stato_audit"], f"Dtypes of dataframe from 'consulenti_stato_audit' does not match with original")

    def test_consulenti_stato_audit_ko(self):

        EDATE  = my_date_ko + timedelta(days = 1)

        SDATE2 = EDATE - timedelta(days = 20)

        connector_di = self.connectors["conn_di"]

        self.assertRaises(Exception, get_consulenti_stato_audit, connector_di, SDATE2, EDATE)

    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #
    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #
    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #
    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #

    def test_bi_tables_ok(self):

        EDATE  = my_date_ok + timedelta(days = 1)
        SDATE1 = EDATE - timedelta(days = 30)

        connector_bi = self.connectors["conn_bi"]
        connector_di = self.connectors["conn_di"]

        orders = get_ft_venduto(connector_bi, SDATE1, EDATE)
        calls = get_ft_chiamate(connector_bi, SDATE1, EDATE)
        user_team_vw = get_user_team_view(connector_di)

        # ----------------------------------------------------------------------

        bi_dataframe = process_bi_tables(calls, orders, user_team_vw)
        self.assertNotEqual(bi_dataframe.shape[0], 0, "'BI Dataframe' has 0 rows")
        self.assertEqual(bi_dataframe.dtypes.to_dict(), bi_dataframes_col_types, "Columns and types of 'BI dataframe' do not match the expected ones")

        # ----------------------------------------------------------------------

        macrocode_datasets = generate_macrocode_datasets(bi_dataframe, user_team_vw)
        self.assertIsNotNone(macrocode_datasets, "'macrocode_datasets' is None")
        self.assertEqual(type(macrocode_datasets), dict, "'macrocode_datasets' is not a dictionary")

        expected_macrocode = ['Energia FA', 'Energia Commerciali', 'Telefonia FA', 'Telefonia Commerciali']
        self.assertEqual(list(macrocode_datasets.keys()), expected_macrocode, "Macrocode in 'macrocode_datasets' are not the expected ones")

        for mc in expected_macrocode:

            df = macrocode_datasets[mc]
            tuples = [tuple(x) for x in df[["data", "id_consulente"]].to_numpy()]
            self.assertEqual(len(set(tuples)), len(tuples), f"The couples ('data', 'id_consulente') are not unique for 'macrocoda' {mc}")

    def test_bi_tables_ko(self):

        EDATE  = my_date_ko + timedelta(days = 1)
        SDATE1 = EDATE - timedelta(days = 30)

        connector_bi = self.connectors["conn_bi"]

        self.assertRaises(Exception, get_ft_venduto, connector_bi, SDATE1, EDATE)
        self.assertRaises(Exception, get_ft_chiamate, connector_bi, SDATE1, EDATE)

        calls = pd.DataFrame()
        orders = pd.DataFrame()
        user_team_vw = pd.DataFrame()

        self.assertRaises(Exception, process_bi_tables, calls, orders, user_team_vw)

    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #
    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #
    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #
    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #

    def test_di_tables_ok(self):

        EDATE  = my_date_ok + timedelta(days = 1)
        SDATE2 = EDATE - timedelta(days = 20)

        connector_di = self.connectors["conn_di"]

        expected_outcomes = ["accettata", "cancellata", "timeout"]

        prenotazioni = get_inbound_prenotazioni(connector_di, SDATE2, EDATE)
        consulenti = get_consulenti_stato_audit(connector_di, SDATE2, EDATE)

        consulenti = sanitize_consulenti(consulenti)
        expectes_states = ['collegato', 'non_disponibile', 'disponibile', 'in_chiamata', 'pausa']
        self.assertEqual(sorted(consulenti["stato"].unique()), sorted(expectes_states), "Values of 'stato' from 'consulenti_stato_audit' are not matching the expected ones")

        # -----------------------------------------------------------------------

        di_dataframe = process_di_tables(consulenti, prenotazioni)
        self.assertIsNotNone(di_dataframe, "DI dataframe is None")
        self.assertNotEqual(di_dataframe.shape[0], 0, "DI dataframe has 0 rows")
        self.assertEqual(di_dataframe.dtypes.to_dict(), di_dataframes_col_types, "Columns and types of 'DI dataframe' do not match the expected ones")

        di_cols = di_dataframe.columns
        self.assertTrue("data" in di_cols and "id_consulente" in di_cols, "Columns 'data' and 'id_consulente' are missing in di_dataframe")

        # check that each couple ("data", "id_consulente") is unique in the di_dataframe
        tuples = [tuple(x) for x in di_dataframe[["data", "id_consulente"]].to_numpy()]
        self.assertEqual(len(set(tuples)), len(tuples), "The couples ('data', 'id_consulente') are not unique in 'Ii dataframe'")

        for prefix in ["prenotazioni_rapporto_su_totale_chiamate_esito", "prenotazioni_rapporto_su_totale_durata_esito"]:
            for outcome in expected_outcomes:

                col = prefix + "_" + outcome
                self.assertIn(col, di_cols, f"Column {col} is missing from di_dataframe")

                di_column = di_dataframe[col]
                self.assertFalse(di_column.isna().any(), f"Column {col} contains NaNs")
                self.assertEqual(di_column.dtype, dtype("float64"), f"Column {col} is not a float64")

        prefix = "consulenti_rapporto_su_totale_stato"
        for state in expectes_states:

            col = prefix + "_" + state
            self.assertIn(col, di_cols, f"Column {col} is missing from di_dataframe")

            di_column = di_dataframe[col]
            self.assertFalse(di_column.isna().any(), f"Column {col} contains NaNs")
            self.assertEqual(di_column.dtype, dtype("float64"), f"Column {col} is not a float64")

    def test_di_tables_ko(self):

        EDATE  = my_date_ko + timedelta(days = 1)

        SDATE2 = EDATE - timedelta(days = 20)

        connector_di = self.connectors["conn_di"]

        self.assertRaises(Exception, get_inbound_prenotazioni, connector_di, SDATE2, EDATE)
        self.assertRaises(Exception, get_consulenti_stato_audit, connector_di, SDATE2, EDATE)

        prenotazioni = pd.DataFrame()
        consulenti = pd.DataFrame()

        self.assertRaises(Exception, process_di_tables, consulenti, prenotazioni)

    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #
    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #
    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #
    # -------------------------------------------------------------------------------------------------------------------------------------------------------- #

    def test_process_ok(self):

        results = process(self.connectors, self.importances, {"date" : my_date_ok}, log = lambda x : print(x))
        self.assertIsNotNone(results, "Results after process are 'None'")

        rankings, timestamps = results

        with open(results_path, "r") as exp_file:
            expectancy = json.load(exp_file)

        self.assertEqual(rankings, expectancy, f"Results after process do not match the known one stored in {results_path}")

    def test_process_ko(self):

        with open(os.devnull, 'w') as f:
            sys.stdout = f
            results = process(self.connectors, self.importances, {"date" : my_date_ko}, log = lambda x : print(x))

        self.assertIsNone(results, "Results after process are not 'None'")