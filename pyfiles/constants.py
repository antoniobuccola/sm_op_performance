from numpy import dtype

# chunck size of the SQL table -> pd.DataFrame conversion
chunk_size = 10000

# names of the tables from which the dataframes are extracted 
original_names = ["ft_venduto", "ft_chiamate", "user_team_vw", "inbound_prenotazioni", "consulenti_stato_audit"]

# shapes of the tables stored in ../data/snapshots/data_04_04_2022_at_2022_04_05.sqlite
shapes = [(26788, 30), (257826, 29), (2196, 6), (123199, 11), (1284246, 6)]

# columns for feature extraction and ranking generation
employed_columns = [

	["call_id", "tipo_prodotto", "stato_redemption", "username_operatore", "tipo_contratto"],
	["data", "call_id", "username_operatore", "coda_in_ingresso"],
	["username", "id"],
	["id_consulente", "esito", "data_creazione", "data_accettazione", "data_cancellazione", "data_timeout"],
	["id_consulente", "stato", "inizio", "fine"]
]

# map from 'code' to 'macrocode'
mapping_c2m = {
                "coda 4 - fattura" : "Energia FA",
                "coda 5 - assistenza" : "Energia FA",
                "coda 1 - subentro" : "Energia Commerciali",
                "coda 5t - assistenza" : "Telefonia FA",
                "coda 2 - volture" :  "Energia Commerciali",
                "coda 2tb - altro" : "Telefonia FA",
                "coda 1tb - contratto" : "Telefonia FA",
                "coda 4t - fattura" : "Telefonia FA",
                "coda 3 - switch" : "Energia Commerciali",
                "coda 1t - nuova linea" : "Telefonia Commerciali",
                "coda 2t - voltura" : "Telefonia Commerciali",
                "coda 3t - switch" : "Telefonia Commerciali",
                "campagna radio" :  "Energia Commerciali"
    }

# engineered features
extracted_features = [
        'RR',
        'perc_ko',
        'consulenti_rapporto_su_totale_stato_collegato',
        'consulenti_rapporto_su_totale_stato_disponibile',
        'consulenti_rapporto_su_totale_stato_in_chiamata',
        'consulenti_rapporto_su_totale_stato_non_disponibile',
        'consulenti_rapporto_su_totale_stato_pausa',
        'prenotazioni_rapporto_su_totale_chiamate_esito_accettata',
        'prenotazioni_rapporto_su_totale_chiamate_esito_cancellata',
        'prenotazioni_rapporto_su_totale_chiamate_esito_timeout',
        'prenotazioni_rapporto_su_totale_durata_esito_accettata',
        'prenotazioni_rapporto_su_totale_durata_esito_cancellata',
        'prenotazioni_rapporto_su_totale_durata_esito_timeout',
        'perc_ko_cumul_avg'
               ]

# correction
real = {
            'BBU_telefonia_04' : 'BBU_telefonia04',
            'BBU_telefonia_02' : 'BBU_telefonia02',
            'BBU_telefonia_03' : 'BBU_telefonia03',
            'BBU_telefonia_05' : 'BBU_telefonia05',
            'ser_energia57' : 'ser_energia1057',
            'ser_energia56' : 'ser_energia1056'
    }

# type of the columns contained in the extracted dataframes
columns_type = [

{
 'index': dtype('int64'),
 'area': dtype('O'),
 'client_id': dtype('O'),
 'nextip_client_id': dtype('O'),
 'order_id': dtype('O'),
 'call_id': dtype('O'),
 'call_center': dtype('O'),
 'fornitore': dtype('O'),
 'prodotto': dtype('O'),
 'tipo_prodotto': dtype('O'),
 'tipo_fornitura': dtype('O'),
 'stato': dtype('O'),
 'data_stipula': dtype('O'),
 'data_creazione': dtype('<M8[ns]'),
 'data_modifica': dtype('<M8[ns]'),
 'modalita_pagamento': dtype('O'),
 'bolletta': dtype('O'),
 'tipo_contratto': dtype('O'),
 'username_operatore': dtype('O'),
 'pod': dtype('O'),
 'pdr': dtype('O'),
 'cf': dtype('O'),
 'telefono': dtype('O'),
 'codice_contratto': dtype('O'),
 'tipo_contatto': dtype('O'),
 'tipo_cliente': dtype('O'),
 'tipo_intestatario': dtype('O'),
 'contratto_recuperato': dtype('int64'),
 'stato_venduto': dtype('O'),
 'stato_redemption': dtype('O')
},
{
 'index': dtype('int64'),
 'id': dtype('int64'),
 'data': dtype('O'),
 'data_creazione': dtype('<M8[ns]'),
 'ora': dtype('int64'),
 'call_id': dtype('O'),
 'area': dtype('O'),
 'client_id': dtype('O'),
 'nextip_client_id': dtype('O'),
 'esito': dtype('O'),
 'esito_macro': dtype('O'),
 'tipologia_esito': dtype('O'),
 'call_center': dtype('O'),
 'username_operatore': dtype('O'),
 'direzione': dtype('O'),
 'telefono': dtype('O'),
 'durata_conversazione': dtype('int64'),
 'durata_attesa': dtype('int64'),
 'durata_chiamata': dtype('int64'),
 'tipologia_chiamata': dtype('O'),
 'tempo_chiusura_cliente': dtype('float64'),
 'tempo_completamento_chiamata': dtype('int64'),
 'campagna': dtype('O'),
 'lista_id': dtype('O'),
 'coda_in_ingresso': dtype('O'),
 'numero_chiamante': dtype('O'),
 'ivr_path': dtype('O'),
 'priorita': dtype('O'),
 'algoritmo_treatment': dtype('O')
},
{
 'index': dtype('int64'),
 'id': dtype('int64'),
 'username': dtype('O'),
 'data_creazione': dtype('<M8[ns]'),
 'id_team': dtype('int64'),
 'nome': dtype('O')
},
{
 'index': dtype('int64'),
 'id': dtype('O'),
 'uuid_chiamata': dtype('O'),
 'id_consulente': dtype('int64'),
 'id_esterno': dtype('O'),
 'esito': dtype('O'),
 'data_creazione': dtype('<M8[ns]'),
 'data_accettazione': dtype('<M8[ns]'),
 'data_timeout': dtype('<M8[ns]'),
 'data_cancellazione': dtype('<M8[ns]'),
 'partner_id': dtype('int64')
},
{
 'index': dtype('int64'),
 'id': dtype('int64'),
 'id_consulente': dtype('int64'),
 'stato': dtype('O'),
 'inizio': dtype('<M8[ns]'),
 'fine': dtype('<M8[ns]')
}
]

# making dictionaries

dict_employed = dict(zip(original_names, employed_columns))
dict_shapes = dict(zip(original_names, shapes))
dict_types = dict(zip(original_names, columns_type))

# column types of dataframe after processing of dataframes from BI database
bi_dataframes_col_types = {
    
    'data': dtype('O'), 
    'username': dtype('O'), 
    'order': dtype('float64'), 
    'ko_order': dtype('float64'), 
    'call': dtype('int64'), 
    'macrocoda': dtype('O'), 
    'coda_in_ingresso': dtype('O'), 
    'num_ord': dtype('float64'), 
    'num_ko_ord': dtype('float64'), 
    'num_cal': dtype('float64'), 
    'RR': dtype('float64'), 
    'perc_ko': dtype('float64')
    
}

# column types of dataframe after processing of dataframes from BI database

di_dataframes_col_types = {
    
    'data': dtype('O'), 
    'id_consulente': dtype('int64'), 
    'consulenti_rapporto_su_totale_stato_collegato': dtype('float64'), 
    'consulenti_rapporto_su_totale_stato_disponibile': dtype('float64'), 
    'consulenti_rapporto_su_totale_stato_in_chiamata': dtype('float64'), 
    'consulenti_rapporto_su_totale_stato_non_disponibile': dtype('float64'), 
    'consulenti_rapporto_su_totale_stato_pausa': dtype('float64'), 
    'prenotazioni_rapporto_su_totale_chiamate_esito_accettata': dtype('float64'), 
    'prenotazioni_rapporto_su_totale_chiamate_esito_cancellata': dtype('float64'), 
    'prenotazioni_rapporto_su_totale_chiamate_esito_timeout': dtype('float64'), 
    'prenotazioni_rapporto_su_totale_durata_esito_accettata': dtype('float64'), 
    'prenotazioni_rapporto_su_totale_durata_esito_cancellata': dtype('float64'), 
    'prenotazioni_rapporto_su_totale_durata_esito_timeout': dtype('float64')

}