import numpy as np
import pandas as pd

from datetime import date
from sqlalchemy import create_engine

# ------------------------------------------------------------------------------------------------------------------------------------------- #

def dump_report(timestamps):
    
    checkpoints = list(timestamps.keys())
    
    print()
    print(f"@ {timestamps['start'].strftime('%H-%M-%S')} Start")
    
    total_duration = 0
    
    for i in range(1, len(checkpoints)):
        
        curr_ckp = checkpoints[i]
        prev_ckp = checkpoints[i-1]
        
        difference = (timestamps[curr_ckp] - timestamps[prev_ckp]).total_seconds()
        
        print(f"From '{prev_ckp}' to '{curr_ckp}' elapsed {difference} seconds")

# ------------------------------------------------------------------------------------------------------------------------------------------- #
    
def load_importances():
    
    macrocode_window_days = {
                             "Energia FA" : 20,
                             "Energia Commerciali" : 18,       
                             "Telefonia FA" : 18, 
                             "Telefonia Commerciali" : 20
    }
    
    importances = {}
    
    for macrocoda, window in macrocode_window.items():
        
        name = macrocoda.lower().replace(" ", "_")
        
        with open(f"../data/importances/importances_window_{window}_macrocoda_{name}_RR_09.json") as f:    
            importances[macrocoda] = json.load(f)
            
    return importances
    
# ------------------------------------------------------------------------------------------------------------------------------------------- #

def generate_operator_ranking(uni_bi_di, importances):
    
    features = [
        'RR', 
        'perc_ko',
        'consulenti_rapporto_su_totale_stato_collegato',
        'consulenti_rapporto_su_totale_stato_disponibile',
        'consulenti_rapporto_su_totale_stato_in_chiamata',
        'consulenti_rapporto_su_totale_stato_non_disponibile',
        'consulenti_rapporto_su_totale_stato_pausa',
        'prenotazioni_rapporto_su_totale_chiamate_esito_accettata',               
        'prenotazioni_rapporto_su_totale_chiamate_esito_cancellata',
        'prenotazioni_rapporto_su_totale_chiamate_esito_timeout',
        'prenotazioni_rapporto_su_totale_durata_esito_accettata',
        'prenotazioni_rapporto_su_totale_durata_esito_cancellata',
        'prenotazioni_rapporto_su_totale_durata_esito_timeout',
        'perc_ko_cumul_avg'
               ]
    
    rankings = {}
    
    for macrocoda, uni_df in uni_bi_di.items():
        
        df_history = uni_df.copy()

        df_history["perc_ko_cumul_avg_last"] = df_history.groupby("id_consulente")["perc_ko_cumul_avg"].transform("last")
        df_history = df_history.drop_duplicates(subset = "id_consulente", keep = "first").reset_index(drop = True)
        df_history = df_history[["id_consulente", "perc_ko_cumul_avg_last"]]

        day_df_history = df_history[["id_consulente", "perc_ko_cumul_avg_last"]] 
        
        df_window = uni_df.copy()

        for feature in features:
            df_window[feature + "_medio"] = df_window.groupby("id_consulente")[feature].transform("mean")

        df_window.drop(columns = features + ["data", "perc_ko"], inplace = True)
        day_df_window = df_window.drop_duplicates(subset = "id_consulente", keep = "first").reset_index(drop = True)
        
        day_df = pd.merge(day_df_history, day_df_window, how = "inner", on = ["id_consulente"])  
        day_df["score"] = day_df.apply(lambda row: scoring(row, importances[macrocoda], f_RR = 0.9), axis = 1)
        
        ranking = day_df[["id_consulente", "score"]].sort_values(by = "score", ignore_index = True, ascending = False)
        
        rankings[macrocoda] = ranking
        
    return ranking
    
# ------------------------------------------------------------------------------------------------------------------------------------------- #    
    
def join_macrocode_bi_and_di_dataframe(macrocode_bi, di_dataframe):
    
    unified_bi_di = {}
    
    di_dataframe["data"] = di_dataframe["data"].astype(str)
    
    for macrocoda, bi_df in macrocode_bi.items():
           
        bi_df["data"] = bi_df["data"].astype(str)
        
        uni_df = pd.merge(bi_df[['data', 'RR', 'perc_ko', 'id_consulente']], 
                          di_df, 
                          how = "inner", 
                          on = ["data", "id_consulente"])
        
        uni_df.drop(columns = ['perc_ko_cumul_sum', 'perc_ko_cumul_count'], inplace = True)
        
        uni_df["data"] = uni_df["data"].apply(pd.to_datetime)
        
        uni_df["perc_ko_cumul_sum"] = uni_df.groupby("id_consulente")["perc_ko"].transform("cumsum")
        uni_df["perc_ko_cumul_count"] = uni_df.groupby("id_consulente")["perc_ko"].transform("cumcount") + 1
        
        uni_df["perc_ko_cumul_avg"] = uni_df["perc_ko_cumul_sum"] / uni_df["perc_ko_cumul_count"]
        uni_df["perc_ko_cumul_avg"] = uni_df["perc_ko_cumul_avg"].fillna(0)
        
        unified_bi_di[macrocoda] = uni_df
        
    return unified_bi_di
    
# ------------------------------------------------------------------------------------------------------------------------------------------- #
    
def generate_macrocode_datasets(bi_dataframe):
    
    macrocode_bi = {}
    
    for macrocoda in bi_dataframe["macrocoda"].unique():
        
        df = bi_dataframe[bi_dataframe["macrocoda"] == macrocoda]
        
        df = df.drop_duplicates(subset = ['data','username'])
        df = df.merge(user_team_vw[['username','id']], on = 'username', how = 'left')
        df = df.rename(columns = {"id":"id_consulente"}).reset_index(drop = True)
        df["perc_ko"] = df["perc_ko"].fillna(0)  
        
        macrocode_bi[macrocoda] = df
        
    return macrocode_bi

# ------------------------------------------------------------------------------------------------------------------------------------------- # 

def process_di_tables(consulenti, prenotazioni):
    
    consulenti["data"] = consulenti["inizio"].dt.date
    consulenti["durata"] = (consulenti["fine"] - consulenti["inizio"]).dt.total_seconds()
    
    report_consultants = report_consultant = featurize_consultants(consulenti)
    
    prenotazioni['data'] = prenotazioni['data_creazione'].dt.date
    
    df_outs = []
    outcomes = sorted(prenotazioni["esito"].unique())
    mapping = dict(zip(outcomes, ["accettazione", "cancellazione", "timeout"]))
    
    for OUT in outcomes:
        df_out = prenotazioni[prenotazioni["esito"] == OUT]
        df_out["durata"] = (df_out[f"data_{mapping[OUT]}"] - df_out["data_creazione"]).dt.total_seconds()
    
    df_outs.append(df_out)
    prenotazioni = pd.concat(df_outs)
    
    report_reservation = featurize_reservations(prenotazioni)

    di_df = pd.merge(report_consultant, report_reservation, how = 'inner', on = ['data', 'id_consulente']) 
    
    return di_df

# ------------------------------------------------------------------------------------------------------------------------------------------- # 

def process_bi_tables(calls, orders, user_team_view):
    
    mapping = {
                "coda 4 - fattura" : "Energia FA",
                "coda 5 - assistenza" : "Energia FA",
                "coda 1 - subentro" : "Energia Commerciali",
                "coda 5t - assistenza" : "Telefonia FA",
                "coda 2 - volture" :  "Energia Commerciali",
                "coda 2tb - altro" : "Telefonia FA",
                "coda 1tb - contratto" : "Telefonia FA",
                "coda 4t - fattura" : "Telefonia FA",
                "coda 3 - switch" : "Energia Commerciali",
                "coda 1t - nuova linea" : "Telefonia Commerciali",
                "coda 2t - voltura" : "Telefonia Commerciali",
                "coda 3t - switch" : "Telefonia Commerciali",
                "campagna radio" :  "Energia Commerciali"
    }
    
    orders['order'] = np.where(orders["stato_redemption"] == 'ok', 1, 0)
    orders['ko_order'] = np.where(orders["stato_redemption"] == 'ko', 1, 0)

    all_redemption = pd.merge(calls, orders, on = 'call_id', how = 'left')
    all_redemption['order'] = all_redemption['order'].fillna(0)
    all_redemption['ko_order'] = all_redemption['ko_order'].fillna(0)
    all_redemption['call'] = 1
    all_redemption["macrocoda"] = all_redemption["coda_in_ingresso"].replace(mapping)
    
    all_redemption = all_redemption[~((all_redemption["username_operatore_x"].isna()) &\
                                      (all_redemption["username_operatore_y"].isna()))]
    
    real = {
            'BBU_telefonia_04' : 'BBU_telefonia04',
            'BBU_telefonia_02' : 'BBU_telefonia02',
            'BBU_telefonia_03' : 'BBU_telefonia03',
            'BBU_telefonia_05' : 'BBU_telefonia05',
            'ser_energia57' : 'ser_energia1057', 
            'ser_energia56' : 'ser_energia1056'
    }
    
    all_redemption["username"] = np.where(all_redemption["username_operatore_x"] == all_redemption["username_operatore_y"], 
                                          all_redemption["username_operatore_x"], 
                                          np.nan)

    all_redemption["username"] = np.where(all_redemption["username_operatore_x"].isna(), 
                                          all_redemption["username_operatore_y"], 
                                          all_redemption["username"])
                                  
    all_redemption["username"] = np.where(all_redemption["username_operatore_y"].isna(), 
                                          all_redemption["username_operatore_x"], 
                                          all_redemption["username"])
                                  
    all_redemption["username"] = np.where(((all_redemption["username"].isna()) &\
                                           (all_redemption["username_operatore_x"] != all_redemption["username_operatore_y"]) &\
                                           (all_redemption["username_operatore_x"].str[:4] + all_redemption["username_operatore_x"].str[-2:] == all_redemption["username_operatore_y"])),
                                          all_redemption["username_operatore_x"], 
                                          all_redemption["username"])
                                  
    all_redemption["username"] = np.where(((all_redemption["username"].isna()) &\
                                           (all_redemption["username_operatore_x"] != all_redemption["username_operatore_y"]) &\
                                           (all_redemption["username_operatore_x"].str[:4] + all_redemption["username_operatore_x"].str[-2:] != all_redemption["username_operatore_y"])),
                                          all_redemption["username_operatore_y"], 
                                          all_redemption["username"])
                                  
    all_redemption["username"] = np.where(((all_redemption["username"].astype(str).str.contains("Pat") | all_redemption["username"].astype(str).str.contains("pat")) & (all_redemption["username"].str.len() == 6)), 
                                          all_redemption["username"].str[:4] + "energia10" + all_redemption["username"].str[-2:], 
                                          all_redemption["username"])

    all_redemption = all_redemption.replace({"username": real})                            
    all_redemption = all_redemption[~all_redemption["username"].isna()]
    all_redemption = all_redemption[all_redemption["username"].isin(user_team_vw["username"])]
                                  
    # computing on a daily base number of orders (ok and ko) and all_redemption rate (RR)
                                  
    all_redemption = all_redemption[["data", "username", "order","ko_order", "call", "macrocoda", "coda_in_ingresso"]]
    
    aggregated_redemption = all_redemption.groupby(["macrocoda", "username","data"])
    all_redemption["num_ord"] = aggregated_redemption["order"].transform("sum")
    all_redemption["num_ko_ord"] = aggregated_redemption["ko_order"].transform("sum")
    all_redemption["num_cal"] = aggregated_redemption["call"].transform("sum")
                                  
    all_redemption["RR"] = all_redemption["num_ord"] / all_redemption["num_cal"]
    all_redemption["perc_ko"] = all_redemption["num_ko_ord"]/(all_redemption["num_ord"] + all_redemption["num_ko_ord"])
    
    return all_redemption

# ------------------------------------------------------------------------------------------------------------------------------------------- # 

def collect_dataframes(connectors, offset):
    
    MYDATE = date.today()
    
    EDATE  = MYDATE + timedelta(days = 1) - timedelta(days = offset)
    SDATE1 = EDATE - timedelta(days = 30) - timedelta(days = offset)
    
    # Note: two 'macrocode' have an optimal window of 20 days, for the other two, such window is 18 days
    SDATE2 = EDATE - timedelta(days = 20) - timedelta(days = offset)
    
    connector_bi = create_engine(connectors["db_bi"], pool_recycle = 3600).connect()
    orders = get_ft_venduto(connector_bi, SDATE1, EDATE)
    calls = get_ft_chiamate(connector_bi, SDATE1, EDATE)
    
    connector_di = create_engine(connectors["db_di"], pool_recycle = 3600).connect()     
    user_team_vw = get_user_team_view(connector_di)
    prenotazioni = get_inbound_prenotazioni(connector_di, SDATE2, EDATE)
    consulenti = get_consulenti_stato_audit(connector_di, SDATE2, EDATE)
    
    return orders, calls, user_team_vw, prenotazioni, consulenti

# ------------------------------------------------------------------------------------------------------------------------------------------------- # 
# ------------------------------------------------------------------------------------------------------------------------------------------------- #
# ------------------------------------------------------------------------------------------------------------------------------------------------- #
# ------------------------------------------------------------------------------------------------------------------------------------------------- # 
# ------------------------------------------------------------------------------------------------------------------------------------------------- #
# ------------------------------------------------------------------------------------------------------------------------------------------------- #
# ------------------------------------------------------------------------------------------------------------------------------------------------- # 
# ------------------------------------------------------------------------------------------------------------------------------------------------- #
# ------------------------------------------------------------------------------------------------------------------------------------------------- #


## --------------- dataframe getters from database - BEGIN

chunk_size = 10000

def get_user_team_view(connector):
    
    user_team_vw = pd.read_sql(f"SELECT * FROM user_team_vw", connector)
    return user_team_vw

def get_ft_chiamate(connector, SDATE, EDATE):
    
    query_calls = f"SELECT data, call_id, username_operatore, coda_in_ingresso FROM ft_chiamate\
                    WHERE data_creazione > '{SDATE}' AND data_creazione < '{EDATE}' AND direzione LIKE 'Inbound'"
    
    calls = pd.concat([chunk for chunk in pd.read_sql(query_calls, connector, chunksize = chunk_size)])
    
    return calls

def get_ft_venduto(connector, SDATE, EDATE):
    
    query_orders = f"SELECT call_id, tipo_prodotto, stato_redemption, username_operatore, tipo_contratto FROM ft_venduto \
                 WHERE (data_creazione > '{SDATE}' AND data_creazione < '{EDATE}') AND (tipo_prodotto LIKE 'Utility')"
    
    orders = pd.concat([chunk for chunk in pd.read_sql(query_orders, connector, chunksize = chunk_size)])
    
    return orders

def get_inbound_prenotazioni(connector, SDATE, EDATE):
    
    query_prenot = f"""SELECT id_consulente, esito, data_creazione, data_accettazione, data_timeout, data_cancellazione FROM inbound_prenotazioni\
                       WHERE esito != "" AND (data_creazione > '{SDATE}' AND data_creazione < '{EDATE}')"""

    prenotazioni = pd.concat([chunk for chunk in pd.read_sql(query_prenot, connector, chunksize = chunk_size)])
    
    return prenotazioni

def get_consulenti_stato_audit(connector, SDATE, EDATE):
    
    l = ['predictive', 'seleziona modalità', 'Non collegato', 'Esito chiamata']
    
    st = ""
    for s in l:
        st += "'" + s + "', " 
    
    stati = st[:-2]
    
    query_consultants = f"SELECT id_consulente, stato, inizio, fine FROM consulenti_stato_audit\
                          WHERE (inizio > '{SDATE}' AND inizio < '{EDATE}') AND\
                          stato NOT IN ({stati})"
    
    consulenti = pd.concat([chunk for chunk in pd.read_sql(query_consultants, connector, chunksize = chunk_size)])
    
    return consulenti

## --------------- dataframe getters from database - END

## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##

def unfold_pivot(summary, col_state, columns):
    
    """In 'values' there are the extracted features chosen this form to apply the "flattening of the columns after pivoting"""
    
    values = columns[3:]
    report = summary.pivot(index = ["data", "id_consulente"], columns = col_state, values = values).fillna(0).reset_index()
    report.columns = ['_'.join(col).rstrip("_") for col in report.columns]
    
    return report

## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##

## --------------- dataframe from consulenti_stato_audit - BEGIN

def featurize_consultants(df):
    
    """
    Feature extraction on each day for a given consultant using the table pulled from consulenti_stato_audit
    Last update: 30th March 2022
    - f1 : fraction of time the consultant spent in a state over total
    """
    
    df["durata_totale"]  = df.groupby(["data", "id_consulente"])["durata"].transform("sum")
    df["durata_totale_stato"]  = df.groupby(["data", "id_consulente", "stato"])["durata"].transform("sum")
    
    f1 = "consulenti_rapporto_su_totale_stato"
    df[f1] = df["durata_totale_stato"] / df["durata_totale"]
    
    my_columns = ["data", "id_consulente", "stato", f1] 
    summary = df[my_columns].drop_duplicates(subset = ["data", "id_consulente", "stato"], keep = "first").reset_index(drop = True)
    
    return unfold_pivot(summary, col_state = "stato", columns = my_columns)
    
## --------------- dataframe from consulenti_stato_audit - END

## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##

## --------------- dataframe from inbound_prenotazioni - BEGIN

def featurize_reservations(df):
    
    df["numero_chiamate"] = df.groupby(["data", "id_consulente"])["id_consulente"].transform("count")
    df["numero_chiamate_esito"] =  df.groupby(["data", "id_consulente", "esito"])["esito"].transform("count")

    f1 = "prenotazioni_rapporto_su_totale_chiamate_esito"
    df[f1] = df["numero_chiamate_esito"] / df["numero_chiamate"]

    df["durata_totale"] = df.groupby(["data", "id_consulente"])["durata"].transform("sum")
    df["durata_totale_esito"] =  df.groupby(["data", "id_consulente", "esito"])["durata"].transform("sum")

    f2 = "prenotazioni_rapporto_su_totale_durata_esito"
    df[f2] = df["durata_totale_esito"] / df["durata_totale"]
 
    my_columns = ["data", "id_consulente", "esito", f1, f2]
    summary = df[my_columns].drop_duplicates(subset = ["data", "id_consulente", "esito"], keep = "first")
    
    return unfold_pivot(summary, col_state = "esito", columns = my_columns)

def featurize_reservations_OLD(df):
    
    """
    In the last N days, i.e. in the dataframe after filtering, averaging on days:
    - how many calls on total call number have a particular state?
    - how much time on total call duration have a particular state?
    """
        
    df["numero_chiamate"] = df.groupby(["id_consulente"])["id_consulente"].transform("count")
    df["numero_chiamate_esito"] =  df.groupby(["id_consulente", "esito"])["esito"].transform("count")

    f1 = "prenotazioni_rapporto_su_totale_chiamate_esito"
    df[f1] = df["numero_chiamate_esito"] / df["numero_chiamate"]
    
    df["durata_totale"] = df.groupby(["id_consulente"])["durata"].transform("sum")
    df["durata_totale_esito"] =  df.groupby(["id_consulente", "esito"])["durata"].transform("sum")

    f2 = "prenotazioni_rapporto_su_totale_durata_esito"
    df[f2] = df["durata_totale_esito"] / df["durata_totale"]
 
    columns = ["id_consulente", "esito", f1, f2]
    values = columns[2:]
    
    summary = df[columns].drop_duplicates(subset = ["id_consulente", "esito"], keep = "first")

    df_pivot = summary.pivot(index = "id_consulente", columns = "esito", values = values).fillna(0)

    df_pivot.columns = ['_'.join(col) for col in df_pivot.columns.values]
    
    return df_pivot.reset_index()

## --------------- dataframe from inbound_prenotazioni - END

## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##

def str_to_date(DATE):
    
    year  = int(DATE[:4])
    month = int(DATE[5:7])
    day   = int(DATE[8:11])
    
    return date(year, month, day)

## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##

# computing scores for different weight of 'RR_medio'

def scoring(row, importances, f_RR):
    
    """
    Score = f_RR * RR_medio + sum from 1 to Nfeats Weighted feature value 
    weight of feature = [(1 - f_RR) / sum of all importances] x importance of the feature
    """
    
    total_importance = sum(importances.values())
    factor = (1 - f_RR) / total_importance
    
    return f_RR*row["RR_medio"] + sum([factor*I*row[col] for col, I in importances.items()])

## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##

def finalizing_dataset(list_of_dfs):
    
    """Concatenate, rename, NaN filling... of a list of dataframes resulting after moving window average"""
    
    final = pd.concat(list_of_dfs, ignore_index = True).rename(columns = {0 : "data"})
    final["data"] = final["data"].fillna(method = 'ffill')
    
    return final.fillna(0)

## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##
## ---------------------------------------------------------------- ##