import numpy as np
import pandas as pd
from datetime import date

def str_to_date(DATE):
    
    year  = int(DATE[:4])
    month = int(DATE[5:7])
    day   = int(DATE[8:11])
    
    return date(year, month, day)

## ------------------------------------------------------------------------------------------------------------------------------------------------ ##
## ------------------------------------------------------------------------------------------------------------------------------------------------ ##
## ------------------------------------------------------------------------------------------------------------------------------------------------ ##

# computing scores for different weight of 'RR_medio'

def scoring(row, importances, f_RR):
    
    """
    Score = f_RR * RR_medio + sum from 1 to Nfeats Weighted feature value 
    weight of feature = [(1 - f_RR) / sum of all importances] x importance of the feature
    """
    
    total_importance = sum(importances.values())
    factor = (1 - f_RR) / total_importance
    
    return f_RR*row["RR_medio"] + sum([factor*I*row[col] for col, I in importances.items()])

## ------------------------------------------------------------------------------------------------------------------------------------------------ ##
## ------------------------------------------------------------------------------------------------------------------------------------------------ ##
## ------------------------------------------------------------------------------------------------------------------------------------------------ ##

to_be_dropped = ["username", "order", "ko_order", "call", "num_ord", "num_ko_ord", "num_cal", "RR", "perc_ko", "id_consulente"]
useful_cols = ["num_ord", "num_ko_ord", "num_cal"]

def results_after_ranking(df_rank, df_coda, top_f):
    
    rank = df_rank.columns[-1]
    day_by_day_results = []
    
    for date in df_rank["data"].unique():
        
        full_day_coda = df_coda[df_coda["data"] == date]
        full_day_rank = df_rank[df_rank["data"] == date]
        
        try:
            assert len(full_day_coda) > 0 and len(full_day_rank) > 0
        except Exception as E:
            continue
    
        common_ids_coda = full_day_coda["id_consulente"].isin(full_day_rank["id_consulente"])
        common_ids_rank = full_day_rank["id_consulente"].isin(full_day_coda["id_consulente"])
        
        day_coda = full_day_coda[common_ids_coda]
        day_rank = full_day_rank[common_ids_rank]
            
        try:
            assert len(day_coda) > 0 and len(day_rank) > 0
        except Exception as E:
            continue
            
        #print(full_day_coda.shape, full_day_rank.shape, day_coda.shape, day_rank.shape)
        
        #### mask for selection of top consultants
        q = np.quantile(day_rank[rank].values, 1 - top_f)
        mask = day_coda["id_consulente"].isin(day_rank[day_rank[rank] >= q]["id_consulente"])

        # getting the number of orders (ok, ko, on total and on top consultants)
 
        for col in useful_cols:
        
            label = col[4:]
            day_coda[f"totale_{label}"] = day_coda[col].sum().astype(int)
            day_coda[f"migliori_{label}"] = day_coda[mask][col].sum().astype(int)
            
        day_coda = day_coda.drop(columns = to_be_dropped)
        day_coda = day_coda.drop_duplicates(subset = "data", ignore_index = True)
        
        # now day_coda is a dataframe with one row and 7 columns: 
        # the date and the number of orders (ok and ko) and of calls for all and top consultants
        
        day_by_day_results.append(day_coda)
        
    return pd.concat(day_by_day_results, ignore_index = True)

## ------------------------------------------------------------------------------------------------------------------------------------------------ ##
## ------------------------------------------------------------------------------------------------------------------------------------------------ ##
## ------------------------------------------------------------------------------------------------------------------------------------------------ ##

def flatten_rows_by_consultant(df, col_status):
    
    """For each id_consulente, given N unique states for col_status and M features, this function creates a N x M columns, 
       each column containing the feature about a particular state"""
    
    flatten_rows = []
    df_cols = df.columns
    IDS = df["id_consulente"].unique()
    
    for ID in IDS:
    
        consulente = df[df["id_consulente"] == ID].reset_index(drop = True)
        
        flatten_rows.append(pd.concat([pd.DataFrame({f"{feature}_{row[col_status]}" : [row[feature]] 
                                                     for feature in df_cols[2:]})
                                       for i, row in consulente.iterrows()], 
                                      axis = 1).reset_index(drop = True))
    
    report = pd.concat(flatten_rows).fillna(0).reset_index(drop = True)
    report = pd.concat([pd.Series(IDS), report], axis = 1).rename(columns = {0 : "id_consulente"})
    
    return report 

## ------------------------------------------------------------------------------------------------------------------------------------------------ ##
## ------------------------------------------------------------------------------------------------------------------------------------------------ ##
## ------------------------------------------------------------------------------------------------------------------------------------------------ ##

def finalizing_dataset(list_of_dfs):
    
    """Concatenate, rename, NaN filling... of a list of dataframes resulting after moving window average"""
    
    final = pd.concat(list_of_dfs, ignore_index = True).rename(columns = {0 : "data"})
    final["data"] = final["data"].fillna(method = 'ffill')
    
    return final.fillna(0)

## ------------------------------------------------------------------------------------------------------------------------------------------------ ##
## ------------------------------------------------------------------------------------------------------------------------------------------------ ##
## ------------------------------------------------------------------------------------------------------------------------------------------------ ##

from collections import defaultdict

def get_durations(row, acc1, acc2):
    
    data = str(row["inizio"].date())
    
    key1 = f"{data}__{row['id_consulente']}"
    key2 = f"{data}__{row['id_consulente']}__{row['stato']}"
    
    d = (row["fine"] - row["inizio"]).total_seconds() 
    acc1[key1] += d
    acc2[key2] += d

def featurize_consultants_NEW(df):
    
    durations1 = defaultdict(float)
    durations2 = defaultdict(float)
    
    for i, row in df.iterrows():
        get_durations(row, durations1, durations2)
    
    columns = {"data" : [], 
               "id_consulente" : [],
               "stato" : [], 
               "rapporto_su_totale_durata_stato" : []}
        
    for k, v in durations2.items():
        
        data, id_con, stato = tuple(k.split("__"))
        
        columns["data"].append(data)
        columns["id_consulente"].append(id_con)
        columns["stato"].append(stato)
        
        durata_totale = durations1[f"{data}__{id_con}"]
        columns["rapporto_su_totale_durata_stato"].append(v/durata_totale)
    
    summary = pd.DataFrame(columns)
    
    """
    In the last N days, i.e. in the dataframe after filtering, averaging on days:
    - what is the fraction of time the consultant spent in a state over total?   
    """
    
    summary["consulenti_rapporto_su_totale_stato"] = summary.groupby(["id_consulente", "stato"])\
                                                                           ["rapporto_su_totale_durata_stato"].transform("mean")
    
    summary = summary.drop(columns = ["data", "rapporto_su_totale_durata_stato"])
    summary = summary.drop_duplicates(subset = ["id_consulente", "stato"]).reset_index(drop = True)
    
    return flatten_rows_by_consultant(summary, col_status = "stato")


##################################################################################à

def featurize_consultants(df):
   
    df["data"] = df["inizio"].dt.date
        
    df["durata"] = (df["fine"] - df["inizio"]).dt.total_seconds()
    
    df["durata_totale"]  = df.groupby(["data", "id_consulente"])["durata"].transform("sum")
    df["durata_totale_stato"]  = df.groupby(["data", "id_consulente", "stato"])["durata"].transform("sum")
    
    df["rapporto_su_totale_durata_stato"] = df["durata_totale_stato"] / df["durata_totale"]
    
    columns = ["data", "id_consulente", "stato", "durata_totale", "rapporto_su_totale_durata_stato"] 

    summary = df[columns].drop_duplicates(subset = ["data", "id_consulente", "stato"], keep = "first").reset_index(drop = True)
        
    #summary["stato"] = summary["stato"].apply(lambda s: s.lower().replace(" ", "_"))
    
    """
    In the last N days, i.e. in the dataframe after filtering, averaging on days:
    - what is the fraction of time the consultant spent in a state over total?   
    """
    
    summary["consulenti_rapporto_su_totale_stato"] = summary.groupby(["id_consulente", "stato"])\
                                                                           ["rapporto_su_totale_durata_stato"].transform("mean")
    
    summary = summary.drop(columns = ["data", "durata_totale", "rapporto_su_totale_durata_stato"])
    summary = summary.drop_duplicates(subset = ["id_consulente", "stato"]).reset_index(drop = True)
    
    return flatten_rows_by_consultant(summary, col_status = "stato")

## ------------------------------------------------------------------------------------------------------------------------------------------------ ##
## ------------------------------------------------------------------------------------------------------------------------------------------------ ##
## ------------------------------------------------------------------------------------------------------------------------------------------------ ##

def featurize_reservations(df):
    
    # feature extraction on global dataframe
    
    df['data'] = df['data_creazione'].dt.date
    
    df_outs = []

    outcomes = sorted(df.esito.unique())
    mapping = dict(zip(outcomes, ["accettazione", "cancellazione", "timeout"]))
    
    """Compute the duration of each outcome by separating the dataframe by outcome and joining them again"""

    for OUT in outcomes:
    
        df_out = df[df["esito"] == OUT]
        df_out["durata"] = (df_out[f"data_{mapping[OUT]}"] - df_out["data_creazione"]).dt.total_seconds()
    
        df_outs.append(df_out)
    
    df = pd.concat(df_outs)

    df["numero_chiamate"] = df.groupby(["data", "id_consulente"])["id_consulente"].transform("count")
    df["numero_chiamate_esito"] =  df.groupby(["data", "id_consulente", "esito"])["esito"].transform("count")
    df["rapporto_su_totale_chiamate_esito"] = df["numero_chiamate_esito"] / df["numero_chiamate"]
    
    df["durata_totale"] = df.groupby(["data", "id_consulente"])["durata"].transform("sum")
    df["durata_totale_esito"] =  df.groupby(["data", "id_consulente", "esito"])["durata"].transform("sum")
    df["rapporto_su_totale_durata_esito"] = df["durata_totale_esito"] / df["durata_totale"]
    
    summary = df.drop_duplicates(subset = ["data", "id_consulente", "esito"], keep = "first")
    
    summary = summary.drop(columns = ["data_creazione", "data_accettazione", "data_timeout", "data_cancellazione", 
                                      "numero_chiamate", "numero_chiamate_esito", 
                                      "durata", "durata_totale", "durata_totale_esito"]).reset_index(drop = True)

    """
    In the last N days, i.e. in the dataframe after filtering, averaging on days:
    - how many calls on total call number have a particular state?
    - how much time on total call duration have a particular state?
    """    
    
    summary["prenotazioni_rapporto_su_totale_chiamate_esito"] = summary.groupby(["id_consulente", "esito"])\
                                                                                      ["rapporto_su_totale_chiamate_esito"].transform("mean")
    
    summary["prenotazioni_rapporto_su_totale_durata_esito"] = summary.groupby(["id_consulente", "esito"])\
                                                                    ["rapporto_su_totale_durata_esito"].transform("mean")
    
    summary = summary.drop(columns = ["data", "rapporto_su_totale_chiamate_esito", "rapporto_su_totale_durata_esito"])
    summary = summary.drop_duplicates(subset = ["id_consulente", "esito"], ignore_index = True)   
    
    return flatten_rows_by_consultant(summary, col_status = "esito")

## ------------------------------------------------------------------------------------------------------------------------------------------------ ##
## ------------------------------------------------------------------------------------------------------------------------------------------------ ##
## ------------------------------------------------------------------------------------------------------------------------------------------------ ##

def featurize_appointments(df):
    
    df["data"] = df["data_appuntamento"].dt.date

    df["numero_stati"] = df.groupby(["data", "id_consulente", "stato"])["stato"].transform("count")
    df["numero_stati_totale"] = df.groupby(["data", "id_consulente"])["stato"].transform("count")
    df["rapporto_su_totale_stato"] = df["numero_stati"] / df["numero_stati_totale"]

    summary = df.drop(columns = "data_appuntamento").drop_duplicates(subset = ["data", "id_consulente", "stato"], keep = "first")
    #summary = summary.drop(columns = "data").reset_index(drop = True)
    
    """
    In the last N days, i.e. in the dataframe after filtering, averaging on days:
    - how many appointments on total number have a particular state?
    """  
    
    summary["appuntamenti_rapporto_su_totale_stato"] = summary.groupby(["id_consulente", "stato"])["rapporto_su_totale_stato"].transform("mean")
    
    summary = summary.drop_duplicates(subset = ["id_consulente", "stato"])\
                     .drop(columns = ["data", "numero_stati", "numero_stati_totale", "rapporto_su_totale_stato"])\
                     .reset_index(drop = True)
    
    return flatten_rows_by_consultant(summary, col_status = "stato")

## ------------------------------------------------------------------------------------------------------------------------------------------------ ##
## ------------------------------------------------------------------------------------------------------------------------------------------------ ##
## ------------------------------------------------------------------------------------------------------------------------------------------------ ##