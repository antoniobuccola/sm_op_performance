import os
import json

from base64 import b64decode
from datetime import datetime
from jsonschema import validate
from sqlalchemy import create_engine

from lib import load_model, process

MODEL_PATH = os.getenv("MODEL_PATH", "./model")
AUTH_TOKEN = os.getenv("AUTH_TOKEN", "5c5ed05742b7b1910d83b98a")
CONN_STRING_DI = os.getenv("CONN_STRING_DI", "sqlite://")
CONN_STRING_BI = os.getenv("CONN_STRING_BI", "sqlite://")

model = load_model(MODEL_PATH)

connectors = {
    "conn_bi": create_engine(CONN_STRING_BI, pool_recycle = 3600).connect(),
    "conn_di": create_engine(CONN_STRING_DI, pool_recycle = 3600).connect()
}

with open( "./schemas/sm_op_performance_v1.0.0.json" ) as f:
    schema = json.load(f)

def handler(event, context):
    context.log(f'received event: {event}')

    token = event["headers"].get("x-auth-key")
    if  token != AUTH_TOKEN:
        context.log(f'wrong token provided [{token}]')
        return {
            "statusCode": 403,
            "body": json.dumps("forbidden")
        }

    mimetype = event["headers"].get("content-type")
    if  mimetype != "application/json":
        context.log(f'unsupported mime type [{mimetype}]')
        return {
            "statusCode": 400,
            "body": json.dumps("unsupported mime type")
        }

    input = {}
    try:
        body = event["body"]
        if event["isBase64Encoded"]:
            body = b64decode(body)

        body = json.loads(body)
        validate(instance=body, schema=schema)

        if body["date"] is None:
            input["date"] = datetime.now()
        else:
            input["date"] = datetime.strptime(body["date"], '%Y-%m-%d')

    except Exception as err:
        context.log(f'invalid request [{err}]')
        return {
            "statusCode": 400,
            "body": json.dumps("bad request")
        }


    scores, timestamps = process(connectors, model, input, log)
    context.log(f'timestamps: {timestamps}')
    return scores
