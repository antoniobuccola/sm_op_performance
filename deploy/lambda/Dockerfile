FROM python:3.9-slim AS base

RUN  apt-get update
RUN  DEBIAN_FRONTEND=noninteractive apt-get install -y \
    wget \
    findutils \
    vim \
    curl \
    libgomp1

RUN  apt-get clean
RUN  apt-get autoclean -y
RUN  apt-get autoremove -y
RUN  rm -rf /var/lib/apt/lists/*
RUN  pip install --upgrade pip

FROM base as deps
RUN pip install \
    --upgrade \
    --no-cache-dir \
    jsonschema \
    awslambdaric

COPY requirements.txt requirements.txt
RUN  pip install \
    --no-cache-dir \
    -r requirements.txt

FROM deps
COPY lib ./lib
COPY model ./model
COPY schemas ./schemas
COPY *.py ./

EXPOSE 80
EXPOSE 443

ARG COMMIT_HASH
RUN test -n "$COMMIT_HASH"
ENV COMMIT_HASH $COMMIT_HASH

ENTRYPOINT [ "/usr/local/bin/python", "-m", "awslambdaric" ]
CMD [ "main.handler" ]