import os
import json

from datetime import datetime
from sqlalchemy import create_engine

from tenia import Handler
from cocoon import Server

from lib import load_model, process

MODEL_PATH = os.getenv("MODEL_PATH", "./model")
CONN_STRING_DI = os.getenv("CONN_STRING_DI", "sqlite://")
CONN_STRING_BI = os.getenv("CONN_STRING_BI", "sqlite://")

class NullMetrics():
    def latency(self, _a, _b, _val):
        pass

class NullHandler(Handler):
    def send(self, obj):
        pass
    def stop(self):
        pass

def make_engine():
    model = load_model(MODEL_PATH)

    async def adapted(connector, input, metrics):
        if input.date is None:
            input.date = datetime.now()
        else:
            input.date = datetime.strptime(input.date, '%Y-%m-%d')

        score = await process(connector, model, input)
        return score

    return adapted

if __name__ == "__main__":
    options = {}
    options["use_ssl"] = False
    options["model_engine"] = make_engine()
    options["audit_handler"] = NullHandler()

    options["connector"] = {}
    options["connector"]["conn_bi"] = create_engine(CONN_STRING_BI, pool_recycle = 3600).connect()
    options["connector"]["conn_di"] = create_engine(CONN_STRING_DI, pool_recycle = 3600).connect()

    with open( "./schemas/sm_op_performance_v1.0.0.json" ) as f:
        options["schema"] = json.load(f)

    server = Server(options)
    server.start_server()
