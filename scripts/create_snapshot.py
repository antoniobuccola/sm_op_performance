import sqlite3
from datetime import date, datetime
from sqlalchemy import create_engine

from ranking_functions import collect_dataframes


print()
print(f"@ {datetime.now()} started")


connectors = {
    "conn_bi" : create_engine("mysql+pymysql://user_ml:$445cpjyW85WJKA7kDan@109.168.101.125:6666/reports_bi", pool_recycle = 3600).connect(),
    "conn_di" : create_engine("mysql+pymysql://user_ml:$445cpjyW85WJKA7kDan@109.168.101.125:6667/di", pool_recycle = 3600).connect(),
}

print()
print(f"@ {datetime.now()} created connectors and importances")

my_date = date(2022, 4, 4)
orders, calls, user_team_vw, prenotazioni, consulenti = collect_dataframes(connectors, my_date)

print()
print(f"@ {datetime.now()} collected dataframes")

date = my_date.strftime('%d_%m_%Y')
connection_string = f"../data/snapshots/data_{date}_at_2022_04_05.sqlite"
sqlite_connector = sqlite3.connect(connection_string, detect_types = sqlite3.PARSE_DECLTYPES)

print()
print(f"@ {datetime.now()} sqlite connector")

orders.to_sql("ft_venduto", sqlite_connector)
calls.to_sql("ft_chiamate", sqlite_connector)

user_team_vw.to_sql("user_team_vw", sqlite_connector)
prenotazioni.to_sql("inbound_prenotazioni", sqlite_connector)
consulenti.to_sql("consulenti_stato_audit", sqlite_connector)

sqlite_connector.close()

print()
print(f"@ {datetime.now()} created database snapshot")
print()