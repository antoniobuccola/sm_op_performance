#!/usr/bin/env bash

conda create -n sm_ops python=3.9 -y
conda init bash
source /root/.bashrc
conda activate sm_ops

conda install mamba -c conda-forge -y
mamba install dvc dvc-s3 -c conda-forge -y

conda install nb_conda_kernels ipykernel -y

# requirements 
mamba install -c conda-forge pymysql seaborn xgboost nose2 -y
mamba install -c anaconda sqlalchemy -y

# --- if you need sqlite3
# apt update
# apt-get sqlite3
# --- and to open a CLI in sqlite3 with a snapshot in path/to/snap.sqlite
# sqlite3 path/to/snap.sqlite