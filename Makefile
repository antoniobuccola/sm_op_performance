.phony: build clean clean-all help
.RECIPEPREFIX = >

SHELL := /bin/bash
BUILDDIR = _build
SRCDIR = pyfiles
MODELDIR = data/models
SERVERDIR = deploy/server
LAMBDADIR = deploy/lambda
PROJECT = sm_op_performance
HASH := $(shell git rev-parse HEAD)

# from https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help:  ## print help information for exposed makefile rules
> @grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

server-image:
> @rm -rf "$(BUILDDIR)"
> @mkdir -p "$(BUILDDIR)"
> @cp -r "$(SERVERDIR)/." "$(BUILDDIR)"
> @cp -r "$(MODELDIR)/" "$(BUILDDIR)/model"
> @cp -r "$(SRCDIR)/" "$(BUILDDIR)/lib"
> @docker build --build-arg COMMIT_HASH=$(HASH) -t "$(PROJECT)_server" -f "$(BUILDDIR)/Dockerfile" $(BUILDDIR)

lambda-image:
> @rm -rf "$(BUILDDIR)"
> @mkdir -p "$(BUILDDIR)"
> @cp -r "$(LAMBDADIR)/." "$(BUILDDIR)"
> @cp -r "$(MODELDIR)/" "$(BUILDDIR)/model"
> @cp -r "$(SRCDIR)/" "$(BUILDDIR)/lib"
> @docker build --build-arg COMMIT_HASH=$(HASH) -t "$(PROJECT)_lambda" -f "$(BUILDDIR)/Dockerfile" $(BUILDDIR)


clean: ## cleanup all artifacts
> @rm -rf $(BUILDDIR)
